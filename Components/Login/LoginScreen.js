import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, TextInput, ScrollView, TouchableOpacity} from 'react-native'
import LoginImage from '../../Assets/Images/login.png'


export default class LoginScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      emailUser: '',
      password: '',
      isWrong: false
    }
  }
  onHandleInputUsername(input){
    this.setState({
      emailUser: input
    })
  }
  
  onHandleInputPassword(input){
    this.setState({
      password: input
    })
  }
  
  
  onHandleLogin(){
    const {navigation} = this.props
    this.state.emailUser !== null && this.state.password !==null
    ? navigation.navigate('Home', {name: this.state.emailUser})
    : this.setState({isWrong: true})
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <View style={{flexDirection: 'row', justifyContent: 'center', alignContent: 'flex-start'}}>
            <Image source={LoginImage} style={styles.LoginImage} />
          </View>
          <View style={{marginTop: 10, paddingHorizontal: 20}}>
            <Text style={{fontSize: 24, fontFamily: 'Roboto'}}>
              Log in to respict
            </Text>
            <TextInput
              placeholder="email / username"
              style={{borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 10}}
              onChangeText={(input) => this.setState({emailUser: input})}

            />
            <TextInput
              placeholder="password"
              style={{borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 10}}
              onChangeText={(input) => this.setState({password: input})}
              secureTextEntry={true}
            />
            <TouchableOpacity
              accessibilityRole="button"
              style={{marginTop: 15, backgroundColor: 'pink'}}
            >
              <Text
                onPress={()=> this.onHandleLogin()}
                style={{textAlign: 'center', paddingVertical: 10, color: 'white', borderRadius: 5, fontSize: 16}}
              >
                Continue
              </Text>
            </TouchableOpacity>
            {this.state.isWrong ? <Text style={{textAlign: 'center',color: 'red'}}>Fill full the field</Text> : null }
          </View>
        </View>
      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  LoginImage: {
    height: 230,
    resizeMode: 'contain'
  }
});