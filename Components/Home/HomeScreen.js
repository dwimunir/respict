import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'

export default class HomeScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      isLoading : true,
      dataSource: {
        urls:{
          regular: '/Assets/Images/login.png'
        }
      }
    }
  }
  componentDidMount(){
    return fetch('https://api.unsplash.com/search/photos?query=natural&client_id=qxgVdG3Cy6M_9WEIDhEqtgX-ENR2piPvuA5O4n1tZQ0')
    .then((ress) => ress.json())
    .then((ressJson)=>{
      this.setState({
        isLoading: false,
        dataSource: ressJson.results
      })
    })
    .catch((error) => console.log(error))
  }

  onHandlePress(id){
    const index = this.state.dataSource[id]
    const { navigation } = this.props

    navigation.navigate('Picture Details',{...index})
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {
          this.state.isLoading === false
            ? (
              <ScrollView>
                {this.state.dataSource.map((data, index) => (
                  <View style={styles.container} key={index}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Image source={{uri: data.user.profile_image.large}} 
                        style={{height: 34, borderRadius: 50, width: 34, margin: 10, borderColor: 'black', borderWidth: 1}} 
                      />
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>{data.user.name} </Text>
                    </View>
                    <TouchableOpacity onPress={() => this.onHandlePress(index)}>
                      <Image source={{uri: data.urls.regular}} style={{height: 200, resizeMode: 'cover'}} />
                    </TouchableOpacity>
                    <View style={{ margin: 5}}>
                      <Text>{data.alt_description} </Text>
                    </View>
                  </View>
                ))}
              </ScrollView>
            )
            : (
                <View style={{flex: 1, backgroundColor: 'pink', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize: 24, color: 'white', fontWeight: 'bold'}}>Loading...</Text>
                </View>
            )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    paddingHorizontal: 10,
    marginVertical: 10, flex: 1, backgroundColor: 'white', padding: 5
  },
})
