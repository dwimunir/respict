import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { set } from 'react-native-reanimated'

export default class PictureDetails extends Component {
  constructor(props){
    super(props)
    this.state = {
      data: null,
      isLoading: true
    }
  }
  componentDidMount(){
    this.setState({
      isLoading: false
    })
  }
  
  render() {
    const {id,
           urls, 
           user,
           alt_description
          } = this.props.route.params
    return (
      this.state.isLoading === false
      ? (
        <ScrollView>
          <View style={styles.container}>
            <Image source={{uri: urls.regular}} style={{height: 300, resizeMode: 'cover'}} />
            <View style={{flexDirection: 'row', alignItems: 'flex-start', marginTop: 5, paddingRight: 50}}>
              <Image source={{uri: user.profile_image.large}} style={styles.image} />
              <View style={{paddingVertical: 10, paddingBottom: 30}}>
                <Text style={{fontWeight: 'bold', fontSize: 16,}}>{user.name} </Text>
                <Text>{alt_description} </Text>              
              </View>
            </View>
          </View>
        </ScrollView>
      )
      : (
        <View style={{flex: 1, backgroundColor: 'pink', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 24, color: 'white', fontWeight: 'bold'}}>Loading...</Text>
        </View>
      )
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  image: {
    width: 40, 
    height: 40, 
    borderRadius: 50, 
    justifyContent: 'center', 
    margin: 10
  }
})
