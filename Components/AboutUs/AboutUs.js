import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import ig from '../../Assets/Images/instagram.svg'
// import Profile from '../../Assets/Images/profile.jpg'

export default class AboutUs extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{alignItems: 'center', marginTop: 20}}>
          <Image 
            source={require('../../Assets/Images/profile.jpg')} 
            style={{width: 150, height: 150, borderRadius: 100}} 
          />
          <View style={{marginTop: 15, alignItems: 'center'}}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>DWI MISBACHUL MUNIR</Text>
            <Text style={{fontWeight: '100', color: 'gray'}}>React Native Developer</Text>
          </View>
          <View style={{marginTop: 10, alignItems: 'center'}}>
            <Text style={{fontSize: 18, marginTop: 10}}>Instagram </Text>
            <Text style={{color: 'pink'}}>@dwi_m_munir</Text>
            <Text style={{fontSize: 18, marginTop: 10}}>Facebook  </Text> 
            <Text style={{color: 'pink'}}>Dwi Misbachul Munir</Text>
            <Text style={{fontSize: 18, marginTop: 10}}>Linkedin </Text>
            <Text style={{color: 'pink'}}>Dwi Misbachul Munir</Text>
            <Text style={{fontSize: 18, marginTop: 10}}>GitHub </Text> 
            <Text style={{color: 'pink'}}>@DwiMunir</Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({})
