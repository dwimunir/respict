
import React, {Component} from 'react';

import LoginScreen from './Components/Login/LoginScreen';
import {View, StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import HomeScreen from './Components/Home/HomeScreen';
import PictureDetails from './Components/PictureDetails/PictureDetails';
import { createDrawerNavigator } from '@react-navigation/drawer';
import AboutUs from './Components/AboutUs/AboutUs';

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()

class App extends Component {
  render() {
    return (
      <NavigationContainer style={styles.container}>
        <Stack.Navigator 
          initialRouteName="Login" 
        >
          <Stack.Screen name='Home' component={Home} options={{headerShown: false}} />
          <Stack.Screen name='Login' component={LoginScreen} options={{headerShown: false}} />
          <Stack.Screen name='Picture Details' component={PictureDetails} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
const Home = () => {
  return(
    <Drawer.Navigator>
      <Drawer.Screen name='Home' component={HomeScreen} />
      <Drawer.Screen name='About Us' component={AboutUs} />
    </Drawer.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default App;
